
public class DrawTriangles
{
	public static void main(String args[])
	{
		//first triangle
		for (int i=1;i<=10;i++)
		{
			int length = 1;
			while(length<=i)
			{
				System.out.print("*");
				length++;
			}
			System.out.println();
		}
		
		System.out.println();//space between triangles
		
		//second triangle
		for (int i=1;i<=10;i++)
		{
			int length = 10;
			while(length>=i)
			{
				System.out.print("*");
				length--;
			}
			System.out.println();
		}
		
		System.out.println();//space between triangles
		
		//third triangle
		for (int i=1;i<=10;i++)
		{
			int length = 2;//skip first line
			while(length<=i)
			{
				System.out.print(" ");
				length++;
			}
			length = 10;
			while(length>=i)
			{
				System.out.print("*");
				length--;
			}
			System.out.println();
		}
		
		System.out.println();//space between triangles
		
		//fourth triangle
		for (int i=1;i<=10;i++)
		{
			int length = 9;
			while(length>=i)
			{
				System.out.print(" ");
				length--;
			}
			length = 1;
			while(length<=i)
			{
				System.out.print("*");
				length++;
			}
			System.out.println();
		}
		
	}
}
